package io.finloop.actuatordemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActuatorDemoApplication {

	public static void main(String[] args) {
		System.out.println("Starting for changes");
		SpringApplication.run(ActuatorDemoApplication.class, args);
	}
}
